FROM ubuntu

RUN apt-get update && apt-get install -y python3 python3-pip curl git \
    && curl -sSL https://install.python-poetry.org | python3 - \
    && curl https://pyenv.run | bash

ENV HOME /root
ENV PATH $HOME/.local/bin:$PATH
ENV PYENV_ROOT $HOME/.pyenv
ENV PATH $PYENV_ROOT/bin:$PATH
